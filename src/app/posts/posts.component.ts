import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from './../interfaces/post';
import { Comment } from './../interfaces/comment';
import { PostsService } from '../posts.service';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts$:Observable<Post>;
  comments$:Observable<Comment>;
  userId:string;
  postId:number;
  message:string;
  panelOpenState = false;

  savePost(title:string,body:string,id:number){
    this.postsService.addPost(this.userId,title,body)
    this.postId = id;
    this.message = "saved for later viewing"
 }

  constructor(private postsService:PostsService, public authService:AuthService) { }

  ngOnInit(): void {
    this.posts$ = this.postsService.getPosts();
    this.comments$ = this.postsService.getComments();
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }

}
