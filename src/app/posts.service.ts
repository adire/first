import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
import { Comment } from './interfaces/comment';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private PostApi = "https://jsonplaceholder.typicode.com/posts/";
  private CommentApi = "https://jsonplaceholder.typicode.com/comments" ;

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  postCollection:AngularFirestoreCollection;

  getPosts():Observable<Post>{
    return this.http.get<Post>(this.PostApi);
  }

  getComments(){
    return this.http.get<Comment>(`${this.CommentApi}`);
  }

  addPost(userId:string,title:string, body:string){
    const post = {title:title, body:body,Like:0};
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  getCollectionPost(userId){
    this.postCollection = this.db.collection(`users/${userId}/posts`);
    return this.postCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }

  deletePost(userId:string , id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

  updateLikes(userId:string, id:string,Like:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
       {
         Like:Like
       }
     )
  }


  

  constructor(private http:HttpClient, private db: AngularFirestore) { }
}
