// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDCD8aVsaq6vsi7EDhPk1wG6vMm8ssYF3E",
    authDomain: "first-adire.firebaseapp.com",
    projectId: "first-adire",
    storageBucket: "first-adire.appspot.com",
    messagingSenderId: "192519013458",
    appId: "1:192519013458:web:5725ef0bf4541878aa4b8b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
